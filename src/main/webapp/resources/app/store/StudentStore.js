/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:55 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.store.StudentStore', {
    extend: 'Ext.data.Store',
    model: 'SpringExtJS.model.Student',
    autoLoad: true,

    proxy: {
        type: 'ajax',
//        url: baseUrl + '/api/student',
        url: studentApiUrl,
        reader: {
            type: 'json'
        }
    }
});