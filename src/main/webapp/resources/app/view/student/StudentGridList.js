/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:49 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.view.student.StudentGridList', {
    extend:'Ext.grid.Panel',
    alias:'widget.studentGridList',
    title:'Students List',
    initComponent:function () {
        var store = Ext.create('SpringExtJS.store.StudentStore');
        Ext.apply(this, {
            store:store,
            columns:[
                {header:'Id', dataIndex:'id', flex:1},
                {header:'First Name', dataIndex:'firstName', flex:1},
                {header:'Last Name', dataIndex:'lastName', flex:1},
                {header:'Sex', dataIndex:'sex', flex:1},
                {
                    xtype:'datecolumn',
                    dataIndex:'birthDate',
                    text:'Birth Date',
                    format:'d F Y'
                },
                {header:'Age', dataIndex:'age', flex:1},
                {header:'Address', dataIndex:'address', flex:1}
            ],
            dockedItems:[
                {
                    xtype:'toolbar',
                    dock:'top',
                    items:[
                        {
                            xtype:'button',
                            text:'New Student',
                            disabled:false,
                            action:'add'
                        },
                        {
                            xtype:'button',
                            text:'Delete Student',
                            disabled:true,
                            action:'delete'
                        }
                    ]
                },
                {
                    xtype:'pagingtoolbar',
                    dock:'bottom',
                    store:store,
                    displayInfo:true,
                    displayMsg:'Displaying Students {0} - {1} of {2}',
                    emptyMsg:'No Students to display'
                }
            ]
        });
        this.callParent(arguments);
    }
});
