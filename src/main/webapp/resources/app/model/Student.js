/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:52 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.model.Student', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name:'id',
            type:'int'
        },
        {
            name:'address',
            type:'string'
        },
        {
            name:'age',
            type:'int'
        },
        {
            name:'firstName',
            type:'string'
        },
        {
            name:'lastName',
            type:'string'
        },
        {
            name:'sex',
            type:'string'
        },
        {
            name:'birthDate',
            type:'date'
        }
    ]
});