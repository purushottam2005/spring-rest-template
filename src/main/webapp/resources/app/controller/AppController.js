/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/3/13
 * Time: 10:46 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('SpringExtJS.controller.AppController', {
    extend:'Ext.app.Controller',
    /**
     * query selector dari component Ext JS.
     */
    refs:[
        {
            ref:'tabpanel',
            selector:'viewport panel tabpanel'
        }
    ],
    /**
     * init..disini mendefinisikan even untuk component yang ada.
     */
    init:function () {
        this.control({
            'button[action=student]':{
                click:this.buttonStudentClick
            }
        })
    },
    showActiveTab:function(displayType){
        var tabpanel = this.getTabpanel();
        var panel = tabpanel.child(displayType);
        if (Ext.isEmpty(panel)) {
            tabpanel.add({
                xtype:displayType,
                tabConfig:{
                    xtype:'tab',
                    closable:true
                }
            });
            panel = tabpanel.child(displayType);
        }
        tabpanel.setActiveTab(panel);
    },
    buttonStudentClick:function () {
        this.showActiveTab('studentGridList');
    }
});
