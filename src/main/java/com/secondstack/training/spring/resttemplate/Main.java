package com.secondstack.training.spring.resttemplate;

import com.secondstack.training.spring.resttemplate.domain.Student;
import com.secondstack.training.spring.resttemplate.service.StudentService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 5/8/13
 * Time: 10:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args) {
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext(
                        "META-INF/spring/applicationContext-resttemplate.xml"
                );
        StudentService studentService = applicationContext.getBean(StudentService.class);
        List<Student> studentList = studentService.findAll();
        System.out.println(studentList);
    }
}
