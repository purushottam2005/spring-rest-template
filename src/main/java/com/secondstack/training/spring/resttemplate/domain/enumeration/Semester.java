package com.secondstack.training.spring.resttemplate.domain.enumeration;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/25/13
 * Time: 10:18 AM
 * To change this template use File | Settings | File Templates.
 */
public enum Semester {
    EVEN, ODD
}
